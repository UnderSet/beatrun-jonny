***Warning***_:_ Very bad code present. Constructive criticism is appreciated.

*i promise i'll update this eventually, and yes i will eventually finish the replay stuff*

# Beatrun | Community edition

*Looking for the replay stuff? This isn't the branch for that. It's on the [`jonny/replays`](https://gitlab.com/UnderSet/beatrun-jonny/tree/jonny/replays) branch instead.*
* [Русский](./README_ru.md) (Outdated, I don't speak Russian)

Infamous parkour addon for Garry's Mod, fully open sourced and maintained by the community.

**READ THE ***ENTIRE*** README BEFORE ASKING QUESTIONS ON THE [beatrun.ru](https://beatrun.ru) DISCORD!**

## Note about Malicious Code
This version *(and [upstream repository](https://github.com/JonnyBro/beatrun))* does not contain any malicious code whatsoever. What it **does** contain is:
* Lua modules for Discord Rich Presence and Steam Presence
* Network connectivity for courses (activates only when you load or upload courses, default domain is `courses.beatrun.ru`)

**All** of this is optional and you may remove all of it.
Modules are located in [/lua/bin](/lua/bin/) and online courses functionality is in [OnlineCourse.lua](/beatrun/gamemodes/beatrun/gamemode/cl/OnlineCourse.lua)

## Manual Installation (reworked later I promise)
~~1. Download this repository [here](https://github.com/JonnyBro/beatrun/archive/refs/heads/master.zip).
2. **Delete the `beatrun` folder in *your_game_folder/garrysmod/addons* if you have one.**
3. Extract the `beatrun` folder to *your_game_folder/garrysmod/addons*.
4. Extract the `lua` folder to *your_game_folder/garrysmod*.~~


## Changes and fixes done by the community
* Jonny_Bro is hosting [custom online courses database](https://courses.beatrun.ru), which is also free and [open source](https://github.com/relaxtakenotes/beatrun-courses-server/) 🤯!
* Actually good FOV behavior that doesn't freak out with ARC9 or stuff!
* New gamemode: Deathmatch
* Actual [Mirror's Edge-like kickglitch](https://www.youtube.com/watch?v=zK5y3NBUStc) (toggleable with `Beatrun_OldKickGlitch`).
* Added an in-game config menu - you can find it in the tool menu, in the *Beatrun* Category.\
**All** of the Beatrun settings can be changed in the configuration menu.
* Localization support.\
English and Russian is supported in this version.
* Added the ability to get off of ladders.
* Added an arrow that shows the next checkpoint.
* Multiplayer Overdrive usage toggle (serverside, `Beatrun_AllowOverdriveInMultiplayer`).
* Some Beatrun HUD color CVars (`Beatrun_HUDTextColor`, `Beatrun_HUDCornerColor`, `Beatrun_HUDFloatingXPColor`).
* Prop spawn without superadmin toggle (`Beatrun_AllowPropSpawn`).
* Grapple ability toggle (`Beatrun_DisableGrapple`).
* *(Optional!)* Disable quickturn without Beatrun hands (`Beatrun_QuickturnHandsOnly`).
* Added small camera punch when diving.
* Added the ability to remove ziplines that created with *Zipline Gun* with `RMB`.
* Implemented Discord Rich Presence using [open source](#credits) module.

## Fixes

* Your SteamID in the right corner is no longer present.
* Fixed some playermodels show up as ERROR.
* Done various tweaks to the Courses Menu (F4).
* Allowed jumping while walking (🤷).
* Fixed leaderboard sorting in gamemodes.
* Fixed grapple usage in courses and gamemodes.
* Fixed a crash in Data Theft when touching Data Bank.
* Fixed an error on course loading.
* Fixed collisions issues. (PvP damage not going through in gamemodes other than Data Theft)
* Tweaked safety roll, now you can roll under things.
* Tweaked some grapple related stuff. Now it moves with the entity it was attached to and other players can see the rope.
* Made it possible to dive to your death =).

## Related

* [Beatrun Reanimated Project](https://github.com/JonnyBro/beatrun-anims), hosted on GitHub.

# Credits
* [All contributors that ever contributed _**at all**_ to this repository](https://gitlab.com/UnderSet/beatrun-jonny/-/commits/main/?ref_type=HEADS).
* **[JonnyBro](https://github.com/JonnyBro)** - Making most, if not all of the fixes in this repository
* **[EarthyKiller127](https://www.youtube.com/channel/UCiFqPwGo4x0J65xafIaECDQ)**, a.k.a ***datæ*** - Making Beatrun *(screw you for adding DRM to a gamemode by the way)*
* **[relaxtakenotes](https://github.com/relaxtakenotes)** - Made all of this possible.
* **[MTB](https://www.youtube.com/@MTB396)** - Beatrun Reanimated project.
* **[Discord Rich Presence](https://github.com/fluffy-servers/gmod-discord-rpc)** by Fluffy Servers.
* **[Steam Presence](https://github.com/YuRaNnNzZZ/gmcl_steamrichpresencer)** by YuRaNnNzZZ.
